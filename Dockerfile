FROM microsoft/dotnet:2.2-sdk
RUN apt-get update && apt-get install openjdk-8-jre -y
RUN dotnet tool install --global dotnet-sonarscanner && echo "export PATH=$PATH:/root/.dotnet/tools" >> $HOME/.bashrc